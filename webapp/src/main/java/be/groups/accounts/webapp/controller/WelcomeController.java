package be.groups.accounts.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class WelcomeController {
    private final BuildProperties buildProperties;

    @Autowired
    public WelcomeController(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @GetMapping
    public String welcome() {
        return buildProperties.getArtifact() + "@" + buildProperties.getVersion();
    }
}
